using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(TableData), editorForChildClasses:false)]
[CanEditMultipleObjects]
[System.Serializable]
public class BoardDrawer : Editor
{
    private TableData gameDataInstance => target as TableData;
    private ReorderableList _dataList;

    private void OnEnable()
    {
        InitializeReordableList(ref _dataList, "searchWords", "Search Words");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        DrawColumnsRows();
        EditorGUILayout.Space();
        ConverToUpperButton();

        if(gameDataInstance.board != null && gameDataInstance.columns > 0 && gameDataInstance.rows > 0)
        {
            DrawBoardTable();
        }

        EditorGUILayout.BeginHorizontal();
        FillUpWithRandomButton();
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
        _dataList.DoLayoutList();

        serializedObject.ApplyModifiedProperties();
        if(GUI.changed)
        {
            EditorUtility.SetDirty(gameDataInstance);
        }
    }

    private void DrawColumnsRows()
    {
        var columnsTemp = gameDataInstance.columns;
        var rowsTemp = gameDataInstance.rows;

        gameDataInstance.columns = EditorGUILayout.IntField("Columns", gameDataInstance.columns);
        gameDataInstance.rows = EditorGUILayout.IntField("Rows", gameDataInstance.rows);

        if((gameDataInstance.columns != columnsTemp || gameDataInstance.rows != rowsTemp) &&
            gameDataInstance.columns > 0 && gameDataInstance.rows > 0)
        {
            gameDataInstance.CreateNewBoard();
        }
    }
    private void DrawBoardTable()
    {
        var tableStyle = new GUIStyle("box");
        tableStyle.padding = new RectOffset(10, 10, 10, 10);
        tableStyle.margin.left = 32;

        var headerColumnStyle = new GUIStyle();
        headerColumnStyle.fixedWidth = 35;

        var columnStyle = new GUIStyle();
        columnStyle.fixedWidth = 50;

        var rowStyle = new GUIStyle();
        rowStyle.fixedHeight = 25;
        rowStyle.fixedWidth = 40;
        rowStyle.alignment = TextAnchor.MiddleCenter;

        var textFieldStyle = new GUIStyle();
        textFieldStyle.normal.background = Texture2D.grayTexture;
        textFieldStyle.normal.textColor = Color.white;
        textFieldStyle.fontStyle = FontStyle.Bold;
        textFieldStyle.alignment = TextAnchor.MiddleCenter;

        EditorGUILayout.BeginHorizontal(tableStyle);    
        for(var x=0; x<gameDataInstance.columns; x++)
        {
            EditorGUILayout.BeginVertical(x == -1 ? headerColumnStyle : columnStyle);
            for(var y = 0; y<gameDataInstance.rows; y++)
            {
                if(x >= 0 && y >= 0)
                {
                    EditorGUILayout.BeginHorizontal(rowStyle);
                    var character = (string)EditorGUILayout.TextArea(gameDataInstance.board[x].row[y], textFieldStyle);
                    if (gameDataInstance.board[x].row[y].Length > 1) {
                        character = gameDataInstance.board[x].row[y].Substring(0, 1);
                    }

                    gameDataInstance.board[x].row[y] = character;
                    EditorGUILayout.EndHorizontal();
                }
            }
            EditorGUILayout.EndVertical();
        }
        EditorGUILayout.EndHorizontal();
    }

    private void InitializeReordableList(ref ReorderableList list, string propertyName, string listLabel)
    {
        list = new ReorderableList(serializedObject, serializedObject.FindProperty(propertyName), true, true, true, true);
        list.drawHeaderCallback = (Rect rect) =>
        {
            EditorGUI.LabelField(rect, listLabel);
        };

        var l = list;

        list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            var element = l.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;

            EditorGUI.PropertyField(new Rect(rect.x, rect.y, EditorGUIUtility.labelWidth, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("word"), GUIContent.none);
        };
    }

    private void ConverToUpperButton()
    {
        if(GUILayout.Button("To Upper"))
        {
            for(var i=0; i<gameDataInstance.columns; i++)
            {
                for(var j=0; j<gameDataInstance.rows; j++)
                {
                    var errorCounter = Regex.Matches(gameDataInstance.board[i].row[j], @"[a-z]").Count;
                    if (errorCounter > 0) {
                        gameDataInstance.board[i].row[j] = gameDataInstance.board[i].row[j].ToUpper();
                    }
                }
            }

            foreach (var searchWord in gameDataInstance.searchWords)
            {
                var errorCounter = Regex.Matches(searchWord.word, @"[a-z]").Count;
                if (errorCounter > 0)
                {
                    searchWord.word = searchWord.word.ToUpper();
                }
            }
        }
    }

    private void FillUpWithRandomButton()
    {
        if (GUILayout.Button("Randomize"))
        {
            for (var i = 0; i < gameDataInstance.columns; i++)
            {
                for (var j = 0; j < gameDataInstance.rows; j++)
                {
                    var errorCounter = Regex.Matches(gameDataInstance.board[i].row[j], @"[a-zA-Z]").Count;
                    if (errorCounter > 0)
                    {
                        string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                        int index = UnityEngine.Random.Range(0, letters.Length);

                        gameDataInstance.board[i].row[j] = letters[index].ToString();
                    }
                }
            }
        }
    }
}
